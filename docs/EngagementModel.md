# Gaia-X Engagement Model / Journey 

Gaia-X is defined by openness and transparency. 
More than 300 organizations from various countries are already involved in the development of Gaia-X. The contributions by so many skilled and competent individuals is a central aspect to ensure that final GAIA-X outcome will be accepted and used by a far-reaching global community. 
Thus, the GAIA-X development will be open for interested organisations to contribute their know how. The Association has set up  - next to its membership a participation model that gives any company or individual interested in the work of Gaia-X the opportunity to contribute.  

## Participation Levels: Orientation, Contribution, Shaping

### Understand what Gaia-X is about: ORIENTATE 

- **Impression of the working progress & Know how:** Interested stakeholders can follow the website and publications as well as events open to public. 
- ~~If they consider contributing they will be invited to an information seminar which kicks of an onboarding process afterwards (see chapter xx). ~~

### Be part of an open Gaia-X Community: CONTRIBUTE 

- **Insights to the technical development of GAIA-X**
Community stakeholder can contribute to the work packages and Open Work Packages 
- **Knowledge Exchange** 
Community stakeholder have limited access to collaboration platform, webinars and deep dives to 
- Limited access to events and activities connected to GAIA-X 

**A precondition to get access to the open information and to contribute to the GAIA-X Community and is the signing of a letter of consent in combination with information on the GAIA-X IPR policy.**  [?]

### Membership in the Association : SHAPING 

- Association Members can shape the future of Gaia-X by playing an active role within the committees of the Association as well as the developing activities of Gaia-X  

- **Lay the technical foundations of of Gaia-X**
Association members get exclusive access to the technical development and will actively take part in the decision taking processes 
- **Wide ranging Know How exchange in between experts**
Association members have access to all collaboration platforms, webinars and deep dives to learn about Gaia-X and to share your experiences 

The conditions regarding the membership can be found in the _Articles of Association – Chapter 2_. [Further Documents e.g. Onboarding packages] 


### Participation Formats and Information Channels

The following graphic show which access is granted on each level to our information material, communications and internal structures 

[tbd]


![](./media/image1.png){width="1.1166458880139982in"
height="1.9658059930008749in"}

Content {#content .TOCHeading}
=======

[1. The GAIA-X Organizational Handbook
4](#the-gaia-x-organizational-handbook)

[2. Organizational bodies of the GAIA-X Foundation
4](#organizational-bodies-of-the-gaia-x-foundation)

[Overview 4](#overview)

[Board of Directors, Management Board
4](#board-of-directors-management-board)

[Technical Committee 5](#technical-committee)

[Technical Committee Responsibilities
5](#technical-committee-responsibilities)

[1. coordinating the technical direction of GAIA-X;
5](#coordinating-the-technical-direction-of-gaia-x)

[Technical Committee Tasks 5](#technical-committee-tasks)

[Working Mode 6](#working-mode)

[Working Groups 7](#working-groups)

[Charter for GAIA-X Working Groups
7](#charter-for-gaia-x-working-groups)

[Working Groups and Task Forces 7](#working-groups-and-task-forces)

[Working Mode for Working Groups and Task Forces
8](#working-mode-for-working-groups)

[Global Adoption Initiatives 8](#global-adoption-initiatives)

[GAIA-X Support through Community Management
9](#gaia-x-support-through-community-management)

[3. Processes and Tools 9](#processes-and-tools)

[Overview 9](#overview-1)

[Meetings 9](#meetings)

[Purpose of meetings 9](#purpose-of-meetings)

[Invitation 10](#invitation)

[Procedure of meetings 10](#procedure-of-meetings)

[Minutes of the meeting 10](#minutes-of-the-meeting)

[International participants 11](#international-participants)

[Communication 11](#communication)

[Responsibilities of the GAIA-X Management Team
11](#responsibilities-of-the-gaia-x-management-team)

[Collaboration Platforms (COYO, GIT, Slack, E-Mail)
11](#collaboration-platforms-coyo-git-slack-e-mail)

[Overview 11](#overview-2)

[Organization of Coyo GroupSpaces and Sub Spaces
12](#organization-of-coyo-working-groups)

[Groups and projects 12](#_Toc51150069)

[Document Categories 13](#document-categories)

[Document status 13](#document-status-stamps-need-to-be-finally-defined)

[Technical Document Development Processes
13](#technical-document-development-processes)

[General Requirements 14](#general-requirements)

[Transition Requests 14](#transition-requests)

[ADR Process 15](#adr-process)

[Classes of Changes 16](#classes-of-changes)

[Maturity Levels of the Recommendations and Specifications
17](#maturity-levels-of-therecommendations-and-specifications)

[Bodies of the GAIA-X recommendation and specification development
process
17](#bodies-of-thegaia-x-recommendation-and-specification-development-process)

[Tasks for the development 18](#tasks-for-the-development)

[Approval Stages 19](#approval-sta-ges)

[Public Publications 20](#public-publications)

[GAIA-X Technical Architecture Document Development Process
20](#gaia-x-technical-architecture-document-development-process)

[Public and internal Publications 20](#public-and-internal-publications)

[4. Templates 20](#templates)

[GAIA-X Founding Members 21](#gaia-x-founding-members)

[21](#section-1)

[da Publications 22](#_Toc51150087)

[22](#_Toc51150088)

The GAIA-X Organizational Handbook
==================================

The GAIA-X Organizational Handbook defines Bodies and Processes for the
GAIA-X Foundation (in incorporation). It shall also document best
practices for the daily work in the Association.

It is linked to the following documents

-   GAIA-X Foundation: "Articles of Association" (AoA)

-   GAIA-X Foundation: "Internal Rules"

-   GAIA-X Foundation: "Community Process"

Organizational bodies of the GAIA-X Foundation
==============================================

Overview
--------

![](./media/image2.png){width="5.955127952755905in"
height="3.5905391513560807in"}

Board of Directors, Management Board
------------------------------------

The Tasks of the Board of Directors and the Management Board are
described in the GAIA-X Articles of Association (GAIA-X AoA):

The Executive Board is responsible for managing the business activities
of the Association. It consists of up to nine honorary members of the
Executive Board:

-   the chairman of the Executive Board who may use the title of
    "President"

-   two deputy chairmen of the Executive Board

-   the treasurer (who is simultaneously the secretary, unless a
    managing director is appointed to act in this capacity)

-   up to five further members of the Executive Board

Technical Committee
-------------------

The the GAIA-X Technical Committee (GAIA-X-TC) governs the relevant
community projects and technical publications/standards.

The GAIA-X Technical Committee consists of:

-   the Chairs and Co-Chairs of the Working Groups

-   the Community Project Leaders (as guests for their specific topics)

-   the GAIA-X CTO

The Technical Committee is chaired by the GAIA-X CTO.

Every member of the GAIA-X Technical Committee can vote on ballots in
the Technical Committee

The Technical Committee:

### Technical Committee Responsibilities

1.  ### coordinating the technical direction of GAIA-X;

2.  approving community project proposals (including, but not limited
    to, incubation, deprecation, and changes to a project's charter or
    scope) in accordance with a project lifecycle document (to be
    developed), approved and maintained by the GAIA-X-TC

3.  designating Top Level Projects as part of the GAIA-X Open Source
    activities and maintain the GAIA-X Release Plan.

4.  creating ad hoc working groups to focus on cross-project technical
    issues or opportunities

5.  communicating with external and industry organizations concerning
    Project technical matters;)

6.  The X-Association Working group appoints representatives to work
    with other open-source or standards communities

7.  

### Technical Committee Tasks

-   establishing community norms, workflows or policies for releases;

-   discussing, seeking consensus, and where necessary, voting on
    technical matters relating to the code base that affect multiple
    projects; and

-   establishing election processes for Maintainers or other leadership
    roles in the technical community that are not within the scope of
    any single project

-   decides on topics, that can cannot be decided in Working Groups

-   decides on topics, that concern more than one Working Group

-   Approves Recommendations in the maturity state Proposed
    Recommendations (Technical Architecture Document, Architecture of
    Standards...)

-   Prepares decision memos for the Board of Directors

-   can be consulted by the Working Groups and Community Projets

-   Build the Release Plan of the GAIA-X Foundation Services and the
    roadmap of the working packages

![](./media/image3.png){width="6.220858486439195in"
height="3.7274923447069117in"}

### Working Mode

The GAIA-X Technical Committee meets on a weekly basis (Friday
12:00-13:00, Skype).

Physical meetings shall be conducted twice a year (location?)

Discussion channel:
[Slack](https://gaia-xworkspace.slack.com/archives/C01BDHLKQNL) / Gitlab
/ Email

Management of Backlog, ADR, Community Projects & Work Packages: GIT

-   The GXF permanent Working Groups shall
    elect [each]{.underline} one (2) Maintainers which become member of
    the TC, and eight (8) voting Contributors shall be elected by the
    GAIA-X-TC.

-   The TC shall approve the process and timing for nominations and
    elections held on an annual basis.

-   GAIA-X-TC projects generally will involve Maintainers and
    Contributors:

1.  Contributors: anyone in the technical community that contributes
    code, documentation or other technical artifacts to the GAIA-X.

2.  Maintainers: Contributors who have the ability to commit code and
    contributions to GAIA-X\'s main branch, e.g. GAIA-X. A Contributor
    may become a Maintainer by majority approval of the existing
    Maintainers.

3.  Participation in GAIA-X through becoming a Contributor and/or
    Maintainer is open to anyone from an GAIA-X member company.

-   The GAIA-X-TC may:

-   establish workflows and procedures for the submission, approval, and
    closure or archiving of projects,

-   establish criteria and processes for the promotion of Contributors
    to Maintainer status, and

-   amend, adjust and refine the roles of Contributors and Maintainers
    listed in Section 2.2., create new roles and publicly document
    responsibilities and expectations for such roles, as it sees fit.

Working Groups
--------------

### Charter for GAIA-X Working Groups 

The majority of the work of the GAIA-X is done in the GAIA-X Working
Groups. A Working Group is an ongoing major workstream in GAIA-X. Ad hoc
working groups to address specific topics can be setup by the TC.

### Working Groups and Task Forces

#### Working Groups Use Cases & Requirements (suspended)

The Working Group Use Case and Requirements focuses on the development
of Use Cases and the methodological support. Therefor it sets up the Use
Case Advisory Board and organizes a Use Case Conference.

Chairs: Heinrich Pettenpohl (Fraunhofer ISST), Gerrit Stöhr (gesis)

#### Working Group Architecture

1\. The Working Group Architecture is organized in a matrix with 4 main
working streams:

-   GAIA-X-RAM development

-   Developers Community

-   Standardization and Liaisons

-   New Topics

2\. Relevant topics are organized orthogonally to the working streams as
Sub Working Groups

Chairs: Andreas Teuscher (Sick)

#### Working Group Certification 

The Working Group Certification focuses on three Working Streams:

1\. Development of Criteria Catalogs for Participants and Core Components

2\. Maintenance of Criteria Catalogs for Participants and Core Components

3\. Definition and Maintenance of the Certification Process

Chairs: Aleksei Resetko (PwC), Monika Huber (Fraunhofer AISEC)

#### Task Force Legal Framework

The Task Force Legal Framework focuses on

-   Consultation of Working Groups for legal aspects

-   description of Legal Framework for the GAIA-X

-   finding and describing missing legal aspects

Chair: Alexander Duisberg

#### Task Force Business Relevance and Ecosystem Building

Missing Mission Statement

Chairs: Missing

#### Working Group Liaisons

To be decided

### Working Mode for Working Groups 

#### Members of the Working Groups 

Member companies of the GAIA-X Foundation can send staff with
appropriate experience and knowledge as members to the Working Groups.

The whole community (including non-members) can participate in the
community forum and also contribute Requests for change; Non members do
not have voting rights.

#### Chair and Co-Chair of the Working Groups and Task Forces

-   The Working Groups elect a chair and co-chair. A simple majority of
    the attendees is required for the election. The Working Groups
    should elect the chair and the co-chair for a (tbd) period.

-   The Chair and the Co-Chair organize the work in the Working Groups.

#### Responsibilities of the GAIA-X Management Board

-   #### The GAIA-X Management Board supports the Working Groups in organizational aspects

<!-- -->

-   Communication within the Working Group, with GAIA-X members, that
    are not in the Working Group and external organization of meetings
    and events

#### 

#### Confidentiality

-   The meetings of the Working Groups are not public and are subject to
    confidentiality rules

-   Invited guests are allowed to participate in the meetings

#### Invited experts

To be done

Global Adoption Initiatives
---------------------------

Regional Hubs / Special Interest groups

![](./media/image4.png){width="6.424776902887139in"
height="2.9018405511811025in"}

The GAIA-X Foundation supports the development of open source community
projects with Hubs and SIGs and defines a common lifecycle for projects;
Community project leads are invited as guests to the technical committee

GAIA-X Support through Community Management
-------------------------------------------

The GAIA-X foundation establishes a common community management support.

Processes and Tools
===================

Overview
--------

This chapter describes the processes and tools for the daily work in the
GAIA-X. It focuses on the organization and procedure for Working Groups
Meetings, the typical use of the communication tools and the creation of
documents.

Onboarding
----------

This chapter describes the onboarding process and its supporter...

![](./media/image5.png){width="5.59405949256343in"
height="2.976469816272966in"}

Meetings
--------

### Purpose of meetings

1.  The GAIA-X Working Groups must meet quarterly in a personal meeting
    (if possible). The purpose of the meeting is the presentation and
    discussion of results of the (sub-) working groups and to enable
    decisions on the maturity status of recommendations and
    specifications. Therefore, all member companies should send staff
    with adequate knowledge and experience to the meetings. 

2.  The Chair of the Working Group is in charge for the organization of
    the meeting (invitation, venue, agenda, minutes of the meeting), for
    prioritizing topics on the planned agenda and for the procedure of
    the meeting. The deputy of the chair supports the chair in these
    tasks.

3.  The meetings have to be prepared reasonable to make the best use of
    the time of the participants and in relation of the current working
    streams and activities in the group.

 

### Invitation 

#### Regular quarterly meetings

1.  The dates of the regular quarterly of Working Groups and Communities
    meetings SHOULD be scheduled one year in advance. The venue of the
    meetings SHOULD be fixed 6 months before the meeting. The dates of
    the regular quarterly meeting MUST be fixed 6 months before the
    meetings. The venue MUST be fixed in the last meeting before the
    meeting. 

2.  A proposed agenda MUST be provided within one week after the
    predecessor meeting. The planned agenda MUST be provided 2 weeks
    before the meeting. 

3.  All meetings MUST be invited via the GAIA-X Social Collaboration
    Platform. The organizers MAY send additional invites to the
    participants (email, letter...).

4.  For planning purposes all invited persons SHOULD indicate their
    attendance one week before the event.

### Procedure of meetings

1.  The meeting is conducted by the chair of the Working Group or its
    deputy. 

2.  In a regular meeting the action items of the last protocol have to
    be inspected. This is documented in the minutes of the meeting.

3.  The chair determines, if the Working Group can make decisions. For
    this purpose, at least 5 member companies must be represented. A
    single majority is necessary for decisions.

 

### Minutes of the meeting

1.  The minutes of the meeting consists of:

    a.  Venue of the meeting

    b.  Date and time of the meeting

    c.  information on the chair of the meting

    d.  Participants

    e.  planned Agenda

    f.  Information on the topics discussed (presentations and documents
        MUST be attached to the minutes)

    g.  Decisions from the Working Group

    h.  Action Items (Description, Responsible, due date) these will be
        entered and managed with Git.

2.  The minutes MUST be available on COYO latest one week after the
    meeting in a draft state. 

3.  The Minutes of the meeting are official, if there is no other
    decision in the following meeting or after 3 months without any
    protest.

4.  The chair of the meeting is responsible for the meeting minutes. The
    Chair MAY delegate this activity to another participant.

### International participants

1.  The language of the meeting and of all documents is English. 

2.  To enable international members to participate in the meetings an
    online access will be made available.

### Communication

1.  The Working Group MAY publish

    a.  Position Papers

    b.  White Papers

    c.  Specifications

    d.  Recommendations

    e.  GAIA-X-Technical Architecture Document releases

2.  The Working Group MAY notify the public about the current state of
    work on Social Media or in journals or other press releases or on
    events, conferences, fairs etc.

3.  The Working Group MAY Organize own public events. 

###  Responsibilities of the GAIA-X Management Team

1.  The management team sends a quarterly information email to the lead
    contacts of all members with the (known) upcoming events in the next
    three months.

2.  The head office supports the chair in the organization of the
    meetings.

Collaboration Platforms (COYO, GIT, Slack, E-Mail)
--------------------------------------------------

### Overview

The members of the GAIA-X Foundation (in incorporation) are distributed
over organizations and countries. COYO, GIT and Slack are the social
collaborations platforms next to the physical meetings of the Working
Group.

-   COYO: https://gaia.coyocloud.com

-   Slack (gaia-xworkspace.slack.com)

-   Gitlab (<https://gitlab.com/gaia-x>

-   Mailing List: (<gaia-x@priv.ovh.net>)

The COYO platform is proving the basic landing pages for the committees
and working groups and it provides the releases documents.

General discussion channels are organized around Slack Channel or
E-Mail. The E-Mail discussions need to be tracked via timeline.

All formal actions and issues are tracked via GitLab. The development of
source code also. GitHub vs. GitLab (GitHub non-profit organization free
account)

**Note: The consolidation of tools is in progress. Discussions about an
aligned SCP (Social Collaboration Platform) is ongoing (Usage of
Nextcloud was aligned between PSB and AB). Issue: Final decision from
foundation is needed.**

### Organization of COYO Working Groups

Every COYO working group must provide the following basic information.
The [landing
page](https://gaia.coyocloud.com/pages/working-packages-ws2/apps/timeline/timeline)
should be structured as follows:  

![](./media/image6.png){width="5.569306649168854in"
height="3.5023031496062993in"}

1.  Name of the Working Group / work package/ Community project

2.  Responsible persons & supporter

3.  Mission Statement

4.  Targets of the group

5.  Milestones and current working plan (if applicable)

6.  Events and meetings

    a.  of the past

    b.  of the future

    c.  time and place of the event

    d.  related Documents

    e.  minutes of the meeting

7.  Event notices of third parties

8.  Official, released documents

9.  Links to the relevant Git projects, Slack channels and E-Mail Lists

**Gitlab / Slack description (structure)**

 

### Document Categories

Categories should be used in a reasonable and reusable manner to
organize the content in every space, sub space, group and project. The
following categories MUST be used, if applicable:

-   Minutes

-   Presentations

-   Working Draft

-   Candidate Recommendation

-   Proposed Recommendation

-   Recommendation

-   Release

-   Candidate Specification

-   Proposed Specification

-   Specification

-   Position Paper

-   White Paper

 

### Document status / Stamps (need to be finally defined) 

1.  ![](./media/image7.png){width="2.5269728783902012in"
    height="1.3693339895013124in"} The following markers can only set by
    the chair of a Working Group or Technical Committee:

    a.  INPUT

    b.  DISCUSSION PAPER

    c.  DRAFT

    d.  APPROVED

2.  Every collaborator MAY mark content for Action and MUST specify the
    action to be taken. The action SHOULD invoke the responsible
    persons.

3.  Every collaborator MUST mark content as reserved, if the content is
    work in progress of the collaborator or a group.

Technical Document Development Processes
----------------------------------------

The process is based on the W3C technical report development process and
is designed to:

-   maximize consensus about the content of stable technical reports

-   ensure high technical and editorial quality

-   promote consistency among specifications

-   earn endorsement by GAIA-X members and the broader community.

 

### General Requirements

Every document published as part of the technical report development
process must clearly indicate its maturity level and must include
information about the status of the document. This status
information must be unique each time a specification is published,\
must state which Working Group developed the specification, must state
how to send comments or file bugs, and where these are recorded,\
must include expectations about next steps, should explain how the
technology relates to existing international standards and related work
inside or outside the GAIA-X Foundation and should explain or link to an
explanation of significant changes from the previous version.

 

### Transition Requests

For all Transition Requests, to advance a specification to a new
maturity level other than Note, the Working Group:

-   must record the group\'s decision to request advancement.
    (Architecture Decision Record)

-   must obtain Editors approval.

-   must provide public documentation of all substantive changes to the
    technical report since the previous publication.

-   must formally address all issues raised about the document since the
    previous maturity level.

-   must provide public documentation of any Formal Objections.

-   should provide public documentation of changes that are not
    substantive.

-   should report which, if any, of the Working Group\'s requirements
    for this document have changed since the previous step.

-   should report any changes in dependencies with other groups.

-   should provide information about implementations known to the
    Working Group.

 

### ADR Process

![](./media/image8.png){width="6.3in"
height="3.6798611111111112in"}

According to the Articles of Association of the GAIA-X Foundation, the
Board of Directors shall set up a Policy Rules Committee and a Technical
Committee. The Board of Directors can set up and delegate powers to
additional working groups, expert (technical) committees and other
initiatives.

The Technical Committee of the GAIA-X Foundation has the power to accept
or reject ADR.

ADR are developed and proposed by the technical working groups set up by
the Board of Directors within the GAIA-X Foundation, as well as the
working groups that are active in regional and topical \"hubs\" within
the larger GAIA-X community.

An ADR draft is disseminated for feedback to the mailing list used for
the technical development of the specification (currently
<gaia-x@priv.ovh.net>).

The feedback time for the draft version is at least 7 days. When a
consensus is reached within the community, the ADR is proposed to the
Technical Committee by sending it to the convener of the Technical
Committee.

When an ADR is proposed, it is given a number-identifier for future
reference and is added to the repository (currently at
<https://gitlab.com/gaia-x/gaia-x-core/gaia-x-core-document-technical-concept-architecture/-/tree/master/architecture_decision_records>).

The Technical Committee comes to a conclusion within 6 weeks of the ADR
being proposed. Decisions must be taken with a 2/3 majority. If no
conclusion is reached within 6 weeks, then the ADR is rejected. If an
ADR is rejected, the Technical Committee responds with a rationale where
it explains the decision.

The status of an accepted or rejected ADR is updated in the repository.

The status change is further communicated over the mailing list. The
accepted ADR are added to the Architecture Document in an Appendix. The
definitions and technical details of the Architecture Document are
corrected in order to reflect the spirit of the accepted ADR.

#### Status and Lifecycle of an ADR

-   draft

    -   The ADR is still work in progress and has not been proposed by a
        Work Package or several work packages.

-   proposed

    -   The ADR has been proposed, but there is no final decision on it
        so far

-   rejects

    -   The ADR has been rejected.

-   accepted

    -   The ADR has been accepted.

-   deprecated

    -   The ADR was once accepted but is no longer effective.

-   superseded

    -   The ADR was once accepted. But the content has been superseded
        by a more recent ADR.

##### Project Decision Records

Projects within GAIA-X, such as the \"minimum viable GAIA\"
implementation can follow the ADR process in a more lightweight manner.
The so-called PDR do not require acceptance by the Technical Committee.

If a PDR is to be transformed into an ADR, it will have draft-status and
is disseminated and commented on the mailing-list as if it was a new
ADR.

### Classes of Changes

This document distinguishes the following 4 classes of changes to a
document. The first two classes of change are considered editorial
changes, the latter two substantive changes. 

1.  No changes to text content These changes include fixing broken
    links, style sheets or invalid markup.

2.  Corrections that do not affect conformance Changes that reasonable
    implementers would not interpret as changing architectural or
    interoperability requirements or their implementation. Changes which
    resolve ambiguities in the specification are considered to change
    (by clarification) the implementation requirements and do not fall
    into this class. Examples of changes in this class include
    correcting non-normative code examples where the code clearly
    conflicts with normative requirements, clarifying informative use
    cases or other non-normative text, fixing typos or grammatical
    errors where the change does not change implementation requirements.
    If there is any doubt or dissent as to whether requirements are
    changed, such changes do not fall into this class.

3.  Corrections that do not add new features These changes may affect
    conformance to the specification. A change that affects conformance
    is one that:\
    makes conforming data, processors, or other conforming agents become
    non-conforming according to the new version, or makes non-conforming
    data, processors, or other agents become conforming, or clears up an
    ambiguity or under-specified part of the specification in such a way
    that data, \
    a processor, or an agent whose conformance was once unclear becomes
    clearly either conforming or non-conforming.

4.  New features Changes that add a new functionality, element, etc.

 

### Maturity Levels of the Recommendations and Specifications

This document differs 4 maturity levels of documents:

1.  **Working Draft:** Proposal of a Working Groups Subgroup to specify
    new features for existing recommendations and specifications or the
    creation of a new recommendation or specification, approved by the
    editor. The originator or subgroup edits the document on Jive.

2.  **Candidate Recommendation:** Proposal approved by the editor for
    discussion in the Working Group. The Working Group discusses the
    document and may decide to return the document to the originator for
    further review. A change request to an existing recommendation or
    specification, that means a substantive change to the document, but
    does not implement new features, will be given to the Working Group
    as Candidate Recommendation.

3.  **Proposed Recommendation:** Proposal accepted and approved by the
    Working group and the editor, to be approved by the Steering
    Committee. The editor or the Steering Committee may decide to return
    the document to the working group for further review. 

4.  **Release:** Current state of work, that is accepted and approved by
    the Working Group, the editor and the Steering Committee. 

 

### Bodies of the GAIA-X recommendation and specification development process

-   **Working Group:** A GAIA-X Working Group, this can be a permanent
    or ad-hoc working group

-   **Originator:** A person from a GAIA-X member company, an GAIA-X
    member company or a group of GAIA-X members that are willing to work
    on a specified content or topic.

-   **Technical Committee:** The GAIA-X Technical Committee

-   **Editor:** A person named by the Working Group, that coordinates
    the development of documents. If no editor is named by the group,
    the chair of the Working group is the editor. 

  ![](./media/image9.png){width="6.3in"
height="3.54375in"}

### Tasks for the development

 

#### Creation of a working Draft

The Working Group decides with the approval of the editor to create a
working draft on a specified content. The originators MUST create
the Working Draft as a new collaborative Document and collaborate on the
content creation. The document category MUST be working draft.

 

#### Creation of a Candidate Recommendation

##### Preparation

The originators decide to raise the maturity of a document to a
Candidate Recommendation. The editor approves the document and marks it
as success.

 

The Working Group discusses and extends the Working Draft if necessary,
the document category MUST be working draft. The originators or the
Working Group MAY create a public position paper with a notice of the
maturity status Working Draft.

 

##### Decision

When the Working Group and the editor approve the Candidate
Recommendation the originators have to stand up for the Candidate
Recommendation. The Working Group MUST decide if the maturity is raised
to Proposed Recommendation, the Proposed Recommendation is \
refused or the Proposed Recommendation must be adapted. The editor
approves the document and marks it as final.

The document category MUST be Candidate Recommendation after the
approval of the Working Group and the editor. The originators or the
Working Group MAY create a public White paper (distributed on COYO and
the official website) with a notice of the maturity status Candidate
Recommendation.

 

#### Creation of a Proposed Recommendation

A Proposed Recommendation is checked by the editor for consistency with
the current Recommendations /Specifications, with Candidate
recommendations and Working Drafts. The Editor MUST notify the Steering
Committee of the new Proposed Recommendation. The Steering Committee MAY
refuse the change of the Maturity Level with tangible Change
Requests. The editor approves the document and marks it as official.

 

The document category MUST be Proposed Recommendation. The originators
or the Working Group MAY create a white paper with a notice of the
maturity status Proposed Recommendation.

 

#### Creation of a Recommendation / Specification

After the Technical Committee has approved the Proposed Recommendation
the Document Category MUST be Recommendation / Specification. The
originators or the Working Group MAY create a white paper with a notice
of the maturity status Recommendation / Specification.

#### Changes to a recommendation /specification

A "Request for Change" (RfC) can be issued by any members of the
Community. It must be raised via the respective GitLab project.  The
Editor MUST inform the Working Group on the Change Request. If the
request is not a substantive change to the recommendation /
specification, the Editor can change the current state of work. If the
Change Request asks for a new feature in a specification /
recommendation, a new Working Draft will be issued and approved by the
editor. If the Change Request is a substantive change but no new feature
will be introduced the corresponding Proposed Recommendation is issued
as a Candidate Recommendation. This refers to the Classes of Changes.

![](./media/image10.png){width="6.3in" height="3.54375in"}

### Approval Sta ges

This document differs 4 decision types working group decisions /
approval, Editors approval, Technical Committee and Board of Directors
approval.

-   **Working Group Decisions and approval**: The Working Group decides
    and approves in a physical or online meeting. A simple majority is
    required to decide or approve on topic. The decision MUST be
    documented in the meeting minutes. To speed up the process a monthly
    online session is set up for this development process. The
    originators have to present their results in order to get a decision
    or an approval from the Working Group.

-   **Editors approval**: The editor approves on o topic and documents
    this in the at COYO or GIT (at the moment). The Editor MAY request a
    ballot in the working group for the approval.

-   **Technical Committee approval**: The Technical Committee decides
    and approves in a physical or online meeting in a ballot. A simple
    majority is required to decide or approve on topic. The decision
    MUST be documented in the meeting minutes. The originators or the
    working group have to present their work in the Technical Committee
    meeting. The Technical Committee MAY speed up the approval by
    convoking an online meeting for this topic.

### Public Publications

The Working Group can make the state of work available to the public as
position paper, white paper or as recommendation /specification.

-   **Position Paper**: A Position Paper MAY be published by the
    originators or the (Sub-) Working Group after the editor's approval.
    The Position Paper MUST state the maturity of the document as
    Working Draft.

-   **White Paper**: The Working Group MAY publish a White Paper. The
    White Paper MUST state the maturity of the document as Candidate
    Proposal or Proposed Recommendation.

GAIA-X Technical Architecture Document Development Process
----------------------------------------------------------

GAIA-X TAD development Process (detail information from above)

Public and internal Publications
--------------------------------

Location for internal publication is COYO. For external the website
data-infrastructure.eu

Templates
=========

-   [Template Minutes of the
    Meeting](https://gaia.coyocloud.com/files/bc9ba8b4-a48a-428e-bb50-770d8ebdce39/faf647b1-32e4-44b0-b765-08523beb52d2/Template%20for%20Use%20Case%20Description%20and%20Determination%20of%20Technical%20Requirements%20for%20a%20MVG%20docx)
    \<Title\> \<Date\>

-   Agenda \<Meeting\>

-   ADR

-   GAIA-X\_Steering\_Committee\_Status\_Report\_Template.pptx

*To learn more, please check out the website for additional information
and our technical architecture and Policy Rules and Architecture of
Standards documentation\
([www.data-infrastructure.eu](http://www.data-infrastructure.eu)).*

 GAIA-X Founding Members {#gaia-x-founding-members .ListParagraph}
========================

![](./media/image11.png){width="6.386503718285215in" height="4.611395450568679in"} {#section-1 .ListParagraph}
================================================================================================

 {#section-2 .ListParagraph}

![](./media/image12.jpg){width="1.51875in" height="2.1479166666666667in"}da Publications {#da-publications .ListParagraph}
======================================================================================================

![Ein Bild, das Text enthält. Automatisch generierte
Beschreibung](./media/image13.jpg){width="1.5192300962379703in"
height="2.140736001749781in"}

![](./media/image14.jpg){width="1.5320516185476816in" height="2.1671456692913385in"} 
---------------------------------------------------------------------------------------------------

# Policy Rules Committee
The Policy Rules Committee (PRC) maintains the Policy Rules for the segments (1) Infrastructure and (2) Application and Data.   

## Responsibilities

- drafting policies and rules related to the activities of the Association,  
- drafting policies and rules related to the activities the GAIA-X Data Ecosystem and its operation (together the "Data Ecosystem Policy Rules"), with a focus on successfully establishing the European GAIA-X Data Space Initiative.  

Policies for the Association inter alia shall comply with Exhibit 4:  GAIA-X Collaboration Chart, Exhibit 5:  GAIA-X Data Space Creation, and Exhibit 6:  GAIA-X Open Usage and Access to Publications, Specifications, Trade Marks and IP.   

On an operational level this includes responsibility to the following work packages and processes: 
[Please check whether this list is comprehensive and correct in content!] 

- Monitoring of relevant regulatory context (Information security, data protection, special vertical regulations…) in dialogue with the GAIA-X community  
- Liaison to legislative institutions and supervision bodies (EDPB, ENISA…)  
- Monitoring of evolution of the relevant technological context in dialogue with the GAIA-X community (especially Architecture of standards project)  
- Liaison to standardization institutions (CEN/CENELEC, ISO, ETSI…)  
- Initiating and monitoring of regular or ad-hoc updating processes of the set of Policies and Rules in the GAIA-X community  
- Initiating and monitoring of public consultations on proposed modifications of the set of Policies and Rules  
- Preparing decision templates for the updating of the Policies and Rules set to be decided upon by the Board of Directors of the GAIA-X foundation  
- Responsible for all documentation on the Policies and Rules evolution process  
- Initiating and supervising the process of review and (if necessary) modification of certification and accreditation based on adopted sets of Policies and Rules 

## Organisational Structure

- The Policy and Rules Committee is chaired by an elected chairperson elected from within their own ranks.  
- The Policy and Rules Committee establishes working groups to structure and distribute the development in reasonable packages: 
  - The Data and Software PRC Working Group shall consist of up to [AMOUNT] Members. The Members of the Data and Software PRC Working Group shall be appointed by the Board of Directors upon proposal by the Technical Committee [not by PRC?]; 
  - The Infrastructure PRC Working Group shall consist of up to [AMOUNT] Members. The Members of the Infrastructure PRC Working Group shall be appointed by the Board of Directors upon proposal by the Technical Committee [not by PRC?]; 

## Working Modes
**Meetings & Documentations**

- The PRC regularly convenes [once a month, every two weeks – what would be lessons learnt to this? 
- All Members shall have access to the relevant decisions and publications of the Board of Directors relating to the standard setting procedures and processes. These information are available and accessible on NEXTCLOUD [LINK] 

Welcome to the Operational Handbook,  
please use the nav-bar to the left to find your topics.

# Introduction

## Purpose of the Manual 
The Operational Handbook provides a framework upon which those who want to engage in the Gaia-X Community and participate in project Gaia-X. 
This Operational Handbook shall guide Gaia-X stakeholders through the structure and processes of the Gaia-X ecosystem. The following chapters describe the organizational structure, responsibilities and working modes of the bodies of the Gaia-X European Association for Data and Cloud AISBL, participation levels and formats as well as management and technical processes within the Gaia-X Association. Additionally, the broader Gaia-X ecosystem is described in its structures as well as in its exchange and co-working mechanism.  

## Vision and Mission
As the impact of data-driven applications on the European economy has grown over the years, emerging digital ecosystems are faced with a variety of challenges that inhibit further development and collaboration. 
The project Gaia-X addresses them through the establishment of data and infrastructure ecosystems according to European values and standards. Gaia-X allows data to become more widely available, as it opens up high-value shared data spaces and datasets across the EU. 
It fosters the creation, formation, roll-out and growth of digital ecosystems that can be commercially leveraged in and across data spaces. Gaia-X drives value, business cases and innovation towards different target groups including consumers, providers and facilitators such as industry, the public sector or academia. 


# Gaia-X organisation

![logo](images/cto orga.png)

# Overview of Processes (Process Landscape)

The processes of the GAIA-X Association can be characterized by the following three categories

- [Management processes](Management_Processes.md) define the overall strategic direction of an organization. It is the parenthesis for all other processes and comprises all strategic and operational planning, management and controlling activities. 
- [Core processes](Core_Processes-development.md) outline the overall purpose of the association - in the case of GAIA-X the development and subsequently the operations and sales of the data & cloud infrastructure. 
 -  [Support processes](Support_Processes.md) contribute to the result in form of a supportive service infrastructure

[Image Process Landscape]

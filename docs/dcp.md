# Digital Collaboration Platform

Our [Digital Collaboration Platform](https://community.gaia-x.eu/), or DCP, is a [NextCloud](https://nextcloud.com/) instance that all Working Group (WG) members have access to.

The platform provides facilities to:

- share and access documents from all the Committees and Working Groups
- edit documents collaboratively, using [Collabora](https://www.collaboraoffice.com/)
- chat with other DCP users

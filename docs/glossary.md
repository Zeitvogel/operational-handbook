# Glossary

## Association

## Body
Generic Term for organisational Elements within Gaia-X (e.g. WGs, OWPs)

## CLA

Contributor License Agreement

## Data Spaces

## Domain

## Ecosystems

## GXFS

Gaia-X Federation Services

## Hub

## IPR

Intellectual Property Rights

## Member

## OSS

Open Source Software

## Participant

## Provider

## Stakeholder

## User

## WG

see [Working Group](#Working Group)

## WP

see [Work Package](#Work Package)

## Working Group

Group open to Gaia-X AISBL Members only.

## Work Package

Group open to everybody.

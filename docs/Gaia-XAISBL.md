# Gaia-X Association

## General Information

### Responsibilities:

Gaia-X – The European Association of Data and Cloud (AISBL) is an international non-profit organization originated in Belgian Law. Practically, this means that the achievement of its purpose must potentially be useful beyond the Belgian territory.  

The Gaia-X Association has defined its purpose to:  

- promote and facilitate a federated data infrastructure and open software infrastructure 
- provide and operate federated cloud services 
- create a common data ecosystem for users and providers in various public, industry and research domains 
- promoting international cooperation in the areas of digital sovereignty, cloud, high-performance computing, edge computing, cyber security and standardisation 

In order to do so the Association is according to _Article 2 of it’s Articles of Association responsible_ for

- the development of core concepts, architectures, open source software and guidelines for the digital infrastructure as part of Gaia-X open source projects; 
- cooperation in relevant areas with other relevant standardisation organisations on a European and international level; 
- development of a governance framework for Gaia-X compliance; 
- monitoring compliance with the governance framework; 
- defining a strategic research, innovation and deployment agenda; 
- supporting existing and initiating new organisations that can hold, operate and 
- further develop anything that may be relevant for the ecosystem of the Association, including, without limitation, a technical network, tools, certification processes and governance; 
- development and support of the Gaia-X ecosystem through the promotion of Gaia-X and the membership of the Association, branding and marketing initiatives and collaboration with other initiatives sharing similar visions; 
- providing a directory of accredited and certified service providers and services; 
- definition and assurance of quality standards and certification standards for service providers for Gaia-X; 
- to establish, maintain, operate, support, cooperate with, maintain close contact with, and otherwise assist other international and national associations, organisations, initiatives, fora and networks, having a purpose similar to any of the purposes of the Association, as well as other associations, organisations, initiatives, fora and networks; and 
- to undertake, alone or with others, joint activities as partner or in any other capacity with – amongst others – international organisations, the institutions of the European Union, regional, local and national governments, authorities and other organisations and corporations, both public and private 

### Organisational Structure
Please take note of the operational framework that displays the character of the legally binding and operational documents that shape the structure and work of the AISBL. They also provide basis for the organisational structure. You can find each document: 

- [Articles of Association](https://community.gaia-x.eu/s/NpfZKQAtpL5F9WA)
- [Internal Rules](https://community.gaia-x.eu/s/Eg34SMRxCdFW4sB)
- [Operational Handbook](https://gaia-x.gitlab.io/gaia-x-community/operational-handbook/)

The AISBL has set up the following organisational structure to fulfil the responsibilities mentioned above: 

- [General Assembly](#general-assembly)
- [Board of Directors](#board-of-Directors)
- [Management Board](#board-of-directors)
- Committees ([Technical](#technical-committee) Committee and [Policy Rules Committee](#policy-rules-committee) with sub working groups) 
- [Advisory Boards](#advisory-boards) (Governmental Advisory Board; General Advisory Board) 

## General Assembly

All members of the GAIA-X Association are represented in the General Assembly. The General Assembly is set up as an organ to reflect the majority vote of all members of the AISBL The General Assembly may discuss any questions or any matters within the scope of the Association.  

### Responsibilities

According to _Article 13 of the Articles of Association_ the General Assembly is thus exclusively responsible to  

- adopt any amendment of the Articles of Association;  
- approve of the budget of the Association and annual accounts;  
- appoint and revocate Directors or Independent Board Members upon proposal of the Board of Directors 
- appoint and revocate Independent Board Members upon proposal of the Board of Directors 
- appointment and revocation of the statutory auditor and determine his/her remuneration 
-  discharge Directors, Independent Board Members and, if any, of the statutory auditor;  
- Deal with any claims of recourse against Directors and Independent Board Members; and 
- dissolute the Association, the allocation of the Association’s net assets (remaining balance) in case of dissolution, and the appoint of one or more liquidator(s). 

### Organisational Structure 

The General Assembly is be composed of all Members of the AISBL. 
The Membership to the is open and accessible to any association, company, corporation, organisation, foundation, federation, or international institution/organisation with a clear interest and a binding commitment to the vision and the purpose of the Association. 
More information on the aspects of membership within the AISBL can be reviewed in _Chapter 2 of the Articles of Associations_. 

### Working Modes 

#### Preparation of the Assembly: 

- The ordinary General Assembly will be held once a year  
- It shall take place within the first six months after the end of the financial year thus from January to June.  
- The Boards of Directors sets the date - in coordination with the Management Board 
- The invitation to the General Assembly will be sent out at least thirty calendar dates before the intended date (via letter or e-mail).  
- The agenda is prepared by [which committee] and published at least [two weeks] before the meeting.  

[Please add further information on the agenda setting process; who can add agenda items, is there a standardized agenda structure] 

- Each Member can participate remotely to the General Assembly via an electronic means of communication made available by the Association. An identification procedure will be implemented.  
- Any extraordinary General Assembly will be organized according to the Article of Association whenever required by the interests of the Association. 

#### Facilitation and Documentation 

- The General Assembly shall be chaired by the chairperson of the Board of Directors or, in the absence of the latter, by the oldest member of the Board of Directors present. The chairperson of the meeting may appoint a secretary of the meeting among the meeting's attendants. 

[Please add further information on the facilitation, will there be a reporting structure (beforehand) or during the meeting, who can raise issues and how? In case of conflicts – what kind of resolutions mechanisms will be in plase] 

- The decisions of the General Assembly will be recorded in minutes and will be available together with the documents attached thereto, in a special register in the [NEXTCLOUD](dcp.md) document platform.  

#### Voting Procedures 

- Please refer to the Articles of Association regarding the voting rights and procedures  

## Board of Directors

### Responsibilities

The Board of Directors has the following exclusive responsibilities 

- perform all acts which are necessary or useful for the realisation of the Association's non-profit purpose, with the exception of those reserved to the General Assembly by law or the Articles of Association. 

- present an annual report about the Associations activities which includes at least information regarding (i) the use of the budget, (ii) the annual amount of the membership fees and the calculation thereof and the (iii) the activities of the Association. 

The Board of Directors furthermore is involved and exclusively responsible for the following decisions: 

#### Critical Membership decisions

The BoD can reject an application for the AISBL. For further information please refer to Article 5.2 (c) of the _Articles of Association_.  

#### Annual Budget Approval

- The BoD wll prepare a proposal for the annual budget of the Association and present it for approval to the members. Members will have the opportunity to access the information and comment on them before the ordinary session of the General Assembly (60 days – 35 days before the GA) on a [virtual platform](dcp.md). 
- The final proposal is published beforehand and presented within the ordinary General Assembly. If the budget is not approved, the BoD is responsible to integrate necessary amendments for a second proposal. 
- he information of the budget will be accessible within the Association’s [Please complete data source]

#### Appointment of Management Board (Secretaries Generals)

- The BoD is responsible to appoint the Secretaries General 
- Therefore it needs to prepare detailed job descriptions 

#### Mandate of Independent Board Members

- Independent Board Members are appointed for particular contributions the latter could deliver in the accomplishment of the nonprofit purpose of the Association 
- They will be integrated in the structures of the BoD with the same voting rights as an Director.  
- [Please add further information on the role &  responsibilities of Independent Board Members and how they will be integrated in the actual work structures – if available] 

### Organisational Structure

#### Appointment, Revocation and Termination of Directors 

- Directors are appointed for a period of for a term of two (2) years. The Directors can be reappointed. 
- Independent Board Members are appointed for up to two (2) years.  
- For information on eligibility criteria, proposal and voting procedures please refer to _Chapter 4 of the Articles of Association_.

### Working Modes

#### Meetings & Documentations 

- The BoD regularly convenes [Please complete periodicity] 
- All Members shall have access to the relevant decisions and publications of the Board of Directors relating to the standard setting procedures and processes. These information are available and accessible on [NEXTCLOUD link].

### Information on the current BoD  

#### Directors (2021 - 2022) 

Please find a short introduction of the current directors on [https://gaia-x.eu/news/gaia-x-association-elects-its-first-board-directors](https://gaia-x.eu/news/gaia-x-association-elects-its-first-board-directors)

#### Independent Board Members (2021 – 2022) 

Please find a short introduction of the current Independent Board Members and their mandate on [https://gaia-x.eu/news/gaia-x-association-elects-its-first-board-directors](https://gaia-x.eu/news/gaia-x-association-elects-its-first-board-directors) 

## Management Board

### Responsibilities

The daily management of the Association is managed by  

#### Chief Executive Officer ("CEO") 

-Making major decisions exept those listed in _Article 5.3 Iinternal Rules_,  
-Manage the overall operations and resources 
-Act as main point of communication between the BoD and the Management Board 

On an operational level this includes responsibility to the following work packages and processes:  
[Please check the operational task and change/add to the list] 

- Creating and implementing the AISBL vision and mission. 
- Leading the development of the Association short- and long-term strategy. 
- Communicating, on behalf of the Association with shareholders, government entities, and the public. 


#### Chief Operating Officer ("COO") 

- responsible for designing and implementing business operations,  
- establishing policies that promote the Association’s culture and vision,  
- and overseeing operations of the Association and the work of executives.  
- The COO shall report to the CEO. 

On an operational level this includes responsibility to the following work packages and processes:  
[Please check the operational task and change/add to the list] 

- Management and Documentation of the operational framework (_Articles of Association_, _internal Rules_)
- Set up Processes and interface to and discussion with national hubs   
- Set up a framework for community contributions together with the CTO
- Setting up and guarantee continuous maintenance of a digital collaboration platform  as well as other digital tools that are needed within the development and operation of GAIA-X 
- Take Charge of any process Change Management/ADR process 
- Set up an operate the onboarding procedure towards the AISBL  
- Internal communication (alignment and information routines: Newsletter, Monthly Call, Presentations…) 
- Marketing / External Communication including [homepage](https://gaia-x.eu/) and online channels, Social Media, Corporate Design 
- Provide a Press spokesman, public relations 
- Coordination of trademark rights 
- Provide Documents, Templates, guidelines and publications where necessary 
- Coordinate internal and external events 

#### Chief Technical Officer ("CTO")

- Responsible for all technical and standards matters related to the purpose of the Association.  
- The CTO shall be the chairperson of the Technical Committee and the open source software (OSS) program. 
- The CTO shall report to the CEO. 

On an operational level this includes responsibility to the following work packages and processes:  
[Please check the operational task and change/add to the list] 

- Set up and operate the necessary processes to develop and operate the Federated Services, create the technical architecture and design the Architecture of Standards 
- Enable the efficient execution of the technical development by drafting an operational structure and defining responsibilities for the technical committee and its sub-structures 
- Manage the program management for the technical committee (draft, maintain, escalate) 
- Establish and implement a change management for technical decisions 
- Report technical progress to the relevant committees (BoD, General Assembly) 
- Establish necessary contacts and cooperation within the GAIA-X Community as well as the GAIA-X Hubs to encourage contributions to the technical development 
- Establish the necessary contacts with standardization bodies  

#### Chief Financial Officer ("CFO")

- Manage the financial actions of the association including the tracing of the cash flow as well as financial planning 
- Analyse financial strengths and weaknesses and propose corrective actions 
- Ensure an accurate reporting in accordance with applicable legislation 
- The CFO shall report to the CEO 

On an operational level this includes responsibility to the following work packages and processes:  
[Please check the operational task and change/add to the list] 

- Collection of membership fees 
- Set up and operate a controlling structure for the association 
- Set up contracts with service agencies and ensure that commitments by the associations are met in a timely manner 

### Organisational Structure

Each member of the Management Board shall be able to set up a supporting office structure.

#### Appointment, Revocation and Termination 

- Members of the Management Board are appointed for a term not exceeding three (3) years.
- Members of the Management Board can be reappointed. 
- For  information on eligibility criteria, proposal and voting procedures please refer to _Chapter 5 of the Articles of Association_.

#### Reporting Structures 

- The Management Board reports to the BoD. 
- The Management Board needs to inform the BoD regularly, on all relevant issues regarding strategy, planning, technical development, satiation of risk and risk management and compliance.

### Working Modes

#### Meetings and Documentations

- The Management Board regularly convenes [every two weeks/ once a months ??] 
- The minutes of the Management Board meetings are publicly available and accessible on NEXTCLOUD (any restrictions?) 
- Within each support office necessary meeting and exchange mechanisms shall be established.  
- All C-Level documents shall be uploaded on NEXTCLOUD. The necessary rules of confidentiality will be established through a stringent role and access management.  

### Information on Current Management Board

CEO: Francesco Bonfiglio
[Please add public information]

COO: Dominik Rohrmus
[Please add public information]

CTO: Pierre Gronlier
[Please add public information]

CFO: Jeanette Fava
[Please add public information]

## Advisory Boards

### The General Advisory Board
#### Responsibilities
The General Advisory Board will advise the Board of Directors and provide (technical) support to the Board of Directors in any matters directly or indirectly related to the non-profit purpose and activities of the Association. 
The General Advisory Board will have no legal power. 

#### Organisational Structure
The General Advisory Board will be composed of an appropriate number of advisors (who may not be members of the Board of Directors and who may, but must not, be Representatives of Members of the Association), such number to be determined by the General Assembly. 

**Appointment:**

- Members of the General Advisory Board will be elected by the General Assembly upon proposal by the Board of Directors.  
- Among others, public representatives, representatives of science, user associations, business and the administration shall be members of the General Advisory Board. 

#### Working Modes
Meetings & Documentations 

- The BoD regularly convenes [once a year?] 
- The General Advisory Board shall appoint a chairperson from amongst its members. 

#### Information on current General Advisory Board
[Please add information]

### The Governmental Advisory Board

#### Responsibilities
The Governmental Advisory Board will advise the Board of Directors and provide support on issues of public policy, and especially where there may be an interaction between the Association’s non-profit purpose and activities and (inter)national laws, policies and regulations. 
It shall, in particular, be given the opportunity sufficiently in advance to comment if changes to the articles of association or the Bylaws are contemplated. 
The Governmental Advisory Board shall only have an advisory role to the Association. 

#### Organisational Structure
**Appointment:**
- The Governmental Advisory Board shall have up to three (3) permanent members and an appropriate number of rotating members from States which are a member of the European Union. 
- The permanent membership shall be open to the European Commission, the Federal Government of Germany and the Government of the French Republic. 

#### Working Modes
Meetings & Documentations 

- The Governmental Advisory Board regularly convenes [once a year?] 

#### Information on current Governmental Advisory Board
[Please add information]

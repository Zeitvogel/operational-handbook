# Core Processes - Development

The Association has taken the following three key elements in their focus for the technical development of the sovereign data infrastructure– a technical architecture, an architecture of standards and the Federations Services. 

- The **Gaia-X Technical Architecture** is the concept of a coherent framework that employs the appro-priate digital processes and necessary information technology to facilitate the interconnection be-tween all participants in the European digital economy – considering European values und standards. The Technical Architecture builds upon existing standards and open technology as well as proven concepts for innovative data exchange and services. It also defines core principles and guidelines as well as core architecture elements.
Please find the latest Version of the Architecture here [DOWNLOAD]
- The **Gaia-X Architecture of Standards (PRAAS)** defines a set of policy rules and architecture of standards to support portability, interoperability and interconnection for infrastructure, applications and data. It provides the necessary link between different ecosystem participants to connect the dif-ferent architecture levels.
Please find the latest Version of the PRAAS here [DOWNLOAD]
- The **Federation Services** Gaia-X identifies the minimum technical requirements and services necessary to operate the federated GAIA-X Ecosystem. In an initial approach, four areas of Federation Services were identified for the technical feasibility: Federated Identity & Trust Services; Federated Catalogues for Providers, Nodes and Services; Sovereign data exchange; Compliance with the applicable legal regulation and policies
Please find the latest Information on Federation Services here [DOWNLOAD]

## Technical Roadmap & Architecture

To guarantee a coherent management of the development processes a Roadmap has been implemented. Moreover, change requests and a systematic discussion and integration of new ideas ensure a consistent output of the various parallel work streams

### Technical Roadmap Process

[Integrate Picture]

### Change Management Process

#### Purpose
Systematic documentation, discussion and decision-making structure for change requests to Technical Architecture Document, the PRAAS, and the Federation Services.

#### Overall requirements
Request for Change (RfC)
- Any community member is entitled to created RfCs and have access to the RfC Kanban. 
- An RfC is represented as an issue in Gitlab. 
- The community and TC has Read-Write permissions at the RfC Kanban. 

ADR/PDR Process 
- An ADR/PDR is represented as a JSON-LD (or Markdown?) file in Gitlab. 
- There is an ADR/PDR Kanban
- The Community has Read-Only rights at the ADR/PDR Kanban. 
- The TC has Read-Write permissions at the ADR/PDR Kanban 

#### Description of the RfC Process 
[Is this process in place as described below? Any adaptions necessary]

1.	Any community member creates an issue within GitLab  
2.	Regarding a defined structure, i.e. Label = Status: For Comment, Title starts with RfC, Description-Template?,
3.	The community member adds the issue to the RfC Board in status “New RfC for Comment”. 
4.	All RfCs in state “New” will be pushed to the next state “RfC for Comment” on a "time-frame" (weekly?) basis. 
5.	Decision about adding the Label ADR or PDR
6.	All RfCs with Label ADR or PDR will be pushed to the next state “RfC for validation & prioritization” 
7.	Decision about validation (no prioritization needed).
8.	All RfCs that have been validated and have a prioritization will be pushed to state “RfC transferred to ADR/PDR”. 
9.	All RfCs in state “transfer” and Label ADR or PDR follow the ADR/PDR process. 

#### Description of ADR/PDR process 

[Is this process in place as described below? Any adaptions necessary]

1.	An ADR or PDR will be created from an RfC (issue) that is already in state “transfer” and has the Label ADR or PDR at the RfC Kanban Board. 
2.	An assigned member creates a new Feature-Branch (naming conv. Tbd) in the Repository  
3.	The member creates an ADR Document according to the defined format. 
4.	GitLab Web-UI à Guidance. 
5.	The TC member commits and pushes the created ADR Document to the Repository in the created Feature-Branch. 
6.	The member sets the RfC issue in the RfC Kanban to closed. 
7.	The member creates a new issue in the ADR Kanban, status “New”, and links the newly created Fea-ture-Branch. 
8.	ADR Kanban Process. 
9.	Member creates Pull-Request to Master Branch. 
10.	Approval 
  - All TC members need to approve – Members can be added to Pull Request and everyone needs to approve in order to merge it. 
  -	Just a number of TC members need to approve – different approval workflow follows – Pull-Request approver is a selected group 

[Insert Graphic]

#### Tools and Resources 
The change Management process is managed via GitLab (Gitflow), to which all community stakeholder can request access based on their role.

###### Git Policies & Roles

- The Association uses a gitflow for the development processes in the Technical Committee and its Working Groups. It has currently chosen GitLab as its DevOps platform. 
- GitLab knows the roles “Owner”, “Maintainer, “Developer”, “Reporter”, “Guest”. Details about the per-missions management in GitLab can be found [here](https://docs.gitlab.com/ee/user/permissions.html). 
- The Association uses a group account. The **CTO** is **Owner** of the GitLab organization account. The CTO can name one person as representative and give this person also the role **Owner** to perform activities in the GitLab account in agreement with the CTO. 
- The association creates a Project “Technical Committee” inside the GitLab group account. The Owner of the Project “Technical Committee” are the “CTO” and the named representative. 
- For every Working Group that the “Technical Committee” starts, a subproject to the project “Technical Committee” is created in the GitLab group account. The chair and co-chair of the Working Group are “Maintainer” of their Working group’s subproject. 
- Members of a Committee are members with role “Developer” for their Committee’s project or subpro-ject. For the “Technical Committee” the “CTO” is responsible for adding members to the project. For any other committee the committee’s “Maintainer” is responsible for adding members to the commit-tee’s subproject. 
- Roles are inherited in GitLab. However, any member of a project or subproject that has inherited the role and is not a member of the committee linked to the project or subproject shall only act as ob-server / “Reporter”. Members of a project that have inherited roles and are not the convenor of the linked committee shall act as “Developers” in the project or subproject. 
- The association can create projects or subprojects for Affiliated Groups and Joined Committees. The “CTO” is responsible for the creation of the project or subproject and can name a “Maintainer” for the GitLab project or subproject in accordance with the previous rules. 
- The “Owner” or “Maintainer” of a project or subproject pushes a master into the project. The person shall do this in agreement with the committee. “Pull requests” are decided by the committee and per-formed by the “Maintainer” in accordance with the decision.
- The bodies of the TC can make use of the voting mechanisms in the gitflow tool for dealing with RfCs, ADRs and other issues.

## Software Engineering Process for AISBL OSS

## Integrating Contributions (see Participation Formats)
Collect information on Exchange mechanisms between Working groups, Work Packages
